# Level 1

1. You have to use the Swift URLSession to access [`data.json`](../data.json) (previously introduced).
2. You have to deserialize json data to objects, to be usable for next levels.
