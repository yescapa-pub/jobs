# Yescapa Vue.js Challenge
This test is part of our hiring process at Yescapa for Vue.js positions.

**Looking for a job? Contact us at jobs@yescapa.com**

## Challenge
During this challenge, you will create an application that browses vehicles, displays them in a list and displays a detailed view.
Each level will focus on one subject of the application.

## Guidelines
The only thing you need outside from this repository is [`https://gitlab.com/-/snippets/2095016/raw`](https://gitlab.com/-/snippets/2095016/raw).
This file is the result of a search API and it contains a sample of vehicles. From these data, you are able to complete all levels.

## Expectations
We expect you to write your code in Vue.js.
We expect you to retrieve [data.json](https://gitlab.com/-/snippets/2095016/raw) through the network (do not import the file in your project).
You are free to use libraries as long as your code does the main job.
You are free to innovate, add bonuses.

## Evaluation
Our criteria:
- Correctness. The application should respect the expectations of each level.
- Clarity. Is the code well-organized, clear and maintainable?
- Robustness. Is the code easy and stable?
- Workflow. How do you manage your repository? Do you setup tools to help you programming?

## Sending your results
Once you are done, please send your results to the person you are talking to.

You can send your GitHub/Gitlab project link or zip your directory and send it via email.
If you do not use Github, don't forget to attach your .git folder.

Good luck!
