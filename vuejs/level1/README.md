# Level 1

1. You have to use the network to access [`data.json`](https://gitlab.com/api/v4/snippets/2095016/raw) (previously introduced).
2. You have to deserialize json data to objects, to be usable for next levels.
