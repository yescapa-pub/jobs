# Level 3

1. You have to open a new screen on the click of a list item.
2. You have to display a detailed view based on the previously selected item.
3. You may design the screen as proposed :

![Mockup](mockup.jpg "Mockup")
